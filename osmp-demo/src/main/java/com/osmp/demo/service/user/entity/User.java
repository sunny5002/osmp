 /*   
 * Project: OSMP
 * FileName: User.java
 * version: V1.0
 */
package com.osmp.demo.service.user.entity;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2016年8月25日 下午8:32:47上午10:51:30
 */
public class User {
	
	private String name;
	
	private String age;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}
	
	

}
