/*   
 * Project: OSMP
 * FileName: UserService.java
 * version: V1.0
 */
package com.osmp.demo.service.user;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年11月28日 下午5:27:33
 */
public interface UserService {

	public String getUserName(String name);

	public String getUserAge(String age);

}
