package com.osmp.web.system.properties.dao;

import com.osmp.web.core.mybatis.BaseMapper;
import com.osmp.web.system.properties.entity.Properties;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年11月21日 下午3:20:51
 */
public interface PropertiesMapper extends BaseMapper<Properties> {

}
